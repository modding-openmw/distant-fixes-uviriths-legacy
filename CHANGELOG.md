## Distant Fixes: Uvirith's Legacy Changelog

#### 1.1

* Fixed incorrect usage of `StartScript` which would cause major FPS issues.

[Download Link](https://gitlab.com/modding-openmw/distant-fixes-uviriths-legacy/-/packages/30745191) | [Nexus Mods](https://www.nexusmods.com/morrowind/mods/53139)

#### 1.0

* Initial release of the mod.

[Download Link](https://gitlab.com/modding-openmw/distant-fixes-uviriths-legacy/-/packages/15709234) | [Nexus Mods](https://www.nexusmods.com/morrowind/mods/53139)
