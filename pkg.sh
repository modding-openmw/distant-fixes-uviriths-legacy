#!/bin/sh
set -eu

file_name=distant-fixes-uviriths-legacy.zip

cat > version.txt <<EOF
Mod version: $(git describe --tags || echo 0-DEV)
EOF

zip --must-match --recurse-paths \
    $file_name \
    DistantFixesUvirithsLegacy.esp \
    CHANGELOG.md \
    LICENSE \
    README.md \
    version.txt

sha256sum $file_name > $file_name.sha256sum.txt
sha512sum $file_name > $file_name.sha512sum.txt
