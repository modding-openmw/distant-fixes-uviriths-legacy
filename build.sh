#!/bin/sh
set -e

name=DistantFixesUvirithsLegacy
delta_plugin convert $name.yaml
mv $name.omwaddon $name.esp
